$(window).ready(function(undefined){
    
    var curIndex = 1; //List current index
    
    /* keyevents for each text styles */
    $('.textareaButton[name="bold"]').click(function(){
        replaceWithReserved(getTypePattern('*b*', '*b*'));
    });
    
    $('.textareaButton[name="italic"]').click(function(){
        replaceWithReserved(getTypePattern('*i*', '*i*'));
    });
    
    $('.textareaButton[name="code"]').click(function(){
        
        replaceWithReserved(getTypePattern('\n   ', ''));
    });
    
    $('.textareaButton[name="list"]').click(function(){
        var content = $('#MainContentBox').val().substr(0, $('#MainContentBox')[0].selectionStart).split('\n');
        if(content[content.length - 1].match(/^\s\s\s[\d]+.\s/)){
            replaceWithReserved(getTypePattern('\n   ' + curIndex + '. ', ''));
            curIndex++;
        } else {
            curIndex = 1;
            replaceWithReserved(getTypePattern('\n   1. ', ''));
            curIndex++;
        }
    });
    
    $('.textareaButton[name="squareList"]').click(function(){
        replaceWithReserved(getTypePattern('\n   - ', ''));
    });
    
    $('.textareaButton[name="image"]').click(function(){
        var background = '<div class="lbBackground" style="height: '+$(document).height()+'px; width: '+$(document).width()+'px; z-index: 1000;">test</div>';
        var panel = '<div class="panel"><input type="button" value="Click me"></div>';
        $('#Wrapper').append(background);
        $('#Wrapper').append(panel);
        
    });
    
    
    /*On event is used for dynamically added elements*/
    $(this).on("click", ".panel", function(){
        alert('worked');
    });
    
    $(this).on("click", ".lbBackground", function(){
        $('.lbBackground').remove();
    });
    
    function replaceWithReserved(pattern){
        var startText;
        var endText;
        var middleText;                                            //middle text
        var caretStart = $('#MainContentBox')[0].selectionStart;        //Where cursor of textarea starts
        var caretEnd = $('#MainContentBox')[0].selectionEnd;            //Where cursor of textarea ends
        var content = $('#MainContentBox').val();
        
        
        if(caretStart === caretEnd){
            //When texts are NOT highlighted
            startText = content.slice(0, caretStart);
            middleText = pattern['right'] + ' here ' + pattern['left'];
            endText = content.slice(caretEnd, content.length);
            content = startText + ' ' + middleText + ' ' + endText;
        } else {
            //When some texts are YES highlighted
            startText = content.slice(0, caretStart);
            middleText = pattern['right'] + ' ' +content.slice(caretStart, caretEnd) + ' ' + pattern['left'];
            endText = content.slice(caretEnd, content.length);
            content = startText + ' ' + middleText + ' '+ endText;
        }
        
        
        
        $('#MainContentBox').val(content);
        //setting cursor position by adding middleText's length
        $('#MainContentBox').scrollTop($('#MainContentBox')[0].scrollHeight);
        setSelectionRange($('#MainContentBox')[0], caretStart + middleText.length + 2, caretStart + middleText.length + 2);
    }
    
    function setSelectionRange(textarea, selectionStart, selectionEnd){
        if(textarea.setSelectionRange){
            textarea.focus();
            textarea.setSelectionRange(selectionStart,selectionEnd);
        } else {
            var range = textarea.createTextRange();
            range.collapse(true);
            range.moveEnd('character', selectionEnd);
            range.moveStart('character', selectionStart);
            range.select();
        }
    }
    
    
    /* Update these functions using AJAX */
    
    function getTypePattern(right, left){
        var typePattern = {
            right: right,
            left: left
        };
        return typePattern;
    }
   
});
