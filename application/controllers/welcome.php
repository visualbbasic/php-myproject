<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
    }

    public function index() {
        $this->load->library('Template/TemplateEngine');
        $this->load->model('DAO/PostListMysqlImpl');
        //print_r(PostListMysqlImpl::_list()->result());
        $this->load->view('admin_main.php');
    }

    public function pages() {
        $this->load->library('Template/TemplateEngine');
        $this->load->view('admin_pages.php');
    }

    public function users() {
        $this->load->library('Template/TemplateEngine');
        $this->load->view('admin_users.php');
    }

    public function posts() {
        $this->load->library('Template/TemplateEngine');
        $this->load->view('adminPost/admin_post.php');
    }

    public function addPost() {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('tags', 'Tags', 'required');
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('mainContent', 'Main content', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('adminPost/admin_post_fail.php');
            } else {
                $this->load->library('Post/PostEngine');
                //echo 'asfasf' . $this->postengine->translate($this->input->post('mainContent'));
                $this->load->view('adminPost/admin_post_success.php');
            }
        } else {
            $this->load->view('admin_invalid_request.php');
        }
    }

}