<?php

class PostTranslateArray extends CI_Model {

    public function getTable() {
        $schema = array(
            array(
                "name" => "bold",
                "regex" => '/\*\*(.*?)\*\*+/i',
                "replaceLeft" => '<b>',
                "replaceRight" => '</b>'
            ),
            array(
                "name" => "italic",
                "regex" => '/\*(.*?)\*+/i',
                "replaceLeft" => '<i>',
                "replaceRight" => '</i>'
            )
        );
        return $schema;
    }

}