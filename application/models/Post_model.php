<?php
class Post_model extends CI_Model{
    private $postTitle;
    private $postBody;
    private $postTags;
    
    public function setPostTitle($title){
        $this->postTitle = $title;
    }
    public function setPostBody($body){
        $this->postBody = $body;
    }
    public function setPostTags($tags){
        $this->postTags = $tags;
    }
    
    public function getPostTitle(){
        return $this->postTitle;
    }
    public function getPostBody(){
        return $this->postTitle;
    }
    public function getPostTags(){
        return $this->postTags;
    }
    
}