<?php

interface PostList{
    public function add($post);
    public function delete($post);
    public function _list();
    public function update($oldPost, $newPost);
}