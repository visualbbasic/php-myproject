<?php

class DAOFactory {

    public function newDAO($dbType) {
        $this->CI = & get_instance();
        switch (strtolower($dbType)) {
            case 'mysql':
                $this->CI->load->library('Database/Mysqlsetup');
                break;
        }
    }

}
