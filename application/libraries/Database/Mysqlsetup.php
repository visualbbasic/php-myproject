<?php

define('DB_CONFIG_PATH', './resources/DBConfig/DB_Default_config.json');
define('DROP_TABLES_PATH', './resources/SQL/mysql_reset_drop.sql');

class Mysqlsetup {

    private $config;

    public function __construct() {
        $CI = & get_instance();
        $CI->load->database($this->getConfig());
    }

    public function setConfig($config) {
        $this->config = $config;
        $CI = & get_instance();
        $CI->load->helper('file');
        write_file(DB_CONFIG_PATH, json_encode($config));
    }

    public function getConfig() {
        if (isset($this->config)) {
            return $this->config;
        } else {
            $CI = & get_instance();
            $CI->load->helper('file');
            $contents = read_file(DB_CONFIG_PATH);
            return json_decode($contents, true);
        }
    }

    public function createTables($prefix) {
        /*
         * Prefix cannot be null or undefined
         * It cannot be empty string or only spaces
         */

        $CI->load->helper('file');
        $CI->load->helper('MysqlCreateStatements_helper');
        $CI->load->helper('MysqlDropStatements_helper');
        $tableNames = array('userGrade', 'email', 'page', 'user', 'post', 'privateMessage', 'comment', 'email');

        //Prefix validation
        if (isset($prefix) && trim($prefix) == '')
            $prefix = '';
        else
            $prefix = $prefix . '_';


        foreach ($tableNames as $table) {
            $CI->db->query(mysqlCreateStatements($table, $prefix));
        }
        foreach ($tableNames as $table) {
            //   $CI->db->query(mysqlDropStatements($table, $prefix));
        }
    }

    public function resetTables($prefix) {
        $CI = & get_instance();
        $CI->load->helper('file');

        return read_file(DROP_TABLES_PATH);
    }

}