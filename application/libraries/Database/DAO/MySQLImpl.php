<?php

require_once('GeneralDAO.php');

class Mysqlimpl implements GeneralDAO {

    public function __construct() {
        $CI =& get_instance();
        $CI->load->library('Database/Mysqlsetup');
        
        $config['hostname'] = "localhost";
        $config['username'] = "dydwo8";
        $config['password'] = "password";
        $config['database'] = "myproject";
        $config['dbdriver'] = "mysql";
        $config['dbprefix'] = "";
        $config['pconnect'] = FALSE;
        $config['db_debug'] = TRUE;
        $config['cache_on'] = FALSE;
        $config['cachedir'] = "";
        $config['char_set'] = "utf8";
        $config['dbcollat'] = "utf8_general_ci";
        
        
        $CI->mysqlsetup->setConfig($config);
        $CI->load->database($CI->mysqlsetup->getConfig());
        $CI->mysqlsetup->createTables("asf");
        
    }

    public function deleteComment($commentID) {
        
    }

    public function deleteGrade($gradeID) {
        
    }

    public function deletePage($pageID) {
        
    }

    public function deletePost($postID) {
        
    }

    public function deleteUser($detail) {
        
    }

    public function getUser($userID) {
        
    }

    public function listComments($postID) {
        
    }

    
    public function listPosts($pageID) {
        
    }

    public function listUserGrades() {
        
    }

    public function retreivePages() {
        
    }

    public function retreivePost($postID) {
        
    }

    public function saveComment($comment) {
        
    }

    public function saveGrade($grade) {
        
    }

    public function savePage($page) {
        
    }

    public function savePost($post) {
        
    }

    public function saveUser($user) {
        
    }

    public function updateComment($comment) {
        
    }

    public function updateGrade($grade) {
        
    }

    public function updatePage($page) {
        
    }

    public function updateUser($user) {
        
    }

}