<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('mysqlDropStatements')) {
    function mysqlDropStatements($tableName, $prefix) {
        
        switch($tableName){
            case 'user':
                return "DROP TABLE {$prefix}user;";
            case 'userGrade':
                return "DROP TABLE {$prefix}userGrade;";
            case 'page':
                return "DROP TABLE {$prefix}page;";
            case 'post':
                return "DROP TABLE {$prefix}post;";
            case 'comment':
                return "DROP TABLE {$prefix}comment;";
            case 'privateMessage':
                return "DROP TABLE {$prefix}privateMessage;";
            case 'email':
                return "DROP TABLE {$prefix}email;";
        }
    }
}
