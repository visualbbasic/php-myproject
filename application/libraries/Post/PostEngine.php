<?php

define('POST_TRANSLATE_TABLE', 'TemplateEngine\\PostTranslateTable.json');

class PostEngine{
    public function translate($text){
        $this->CI =& get_instance();
        //loads model that contains array of translate requirements for post mainbody
        $this->CI->load->model('PostTranslateArray');
        foreach (PostTranslateArray::getTable() as $element) {
            $text = $this->invokeFunctions($text, $element);
        }
        return $text;
    }
    
    
    private function invokeFunctions($text, $element){
        $regex = $element['regex'];
        preg_match_all($regex, $text, $array);
        for($i = 0; $i < count($array[0]); $i++){
            $text = str_replace($array[0][$i], $element['replaceLeft'] . $array[1][$i] . $element['replaceRight'], $text);
        }
        return $text;
    }
    
    
}