<?php

define('TRANSLATE_TABLE', 'TemplateEngine\\TranslateTable.json');

class TemplateEngine {

    public function __construct() {
        /* require_once($_SERVER['DOCUMENT_ROOT'].'\\FirePHP\\FirePHP_SETUP.php');

          $firephp->log('constructor invoked'); */
    }

    public function translate($file) {
        $this->CI =& get_instance();
        $this->CI->load->helper('file');
        
        //Get json file contents for translating tamplate and its custom tags
        $translate_table_contents = file_get_contents(getcwd() . '\resources\\' . TRANSLATE_TABLE);
        $template_schama = json_decode($translate_table_contents);
        $file_content = file_get_contents($file);

        
        foreach ($template_schama->table as $element) {
            $file_content = $this->invokeFunctions($file_content, $element);
        }

        //$html = "hello  {test(2, 3)} {test(2, 4)} {test(3,2)} {test(3,76)} {haha(3)}  hello";

        return $file_content;
    }

    private function invokeFunctions($file_content, $element) {
        //original = /{{$element->key}(?:\((?:(\d)+(?:,\s?([^\)]+))?)?\))?}/i
        ///test\((\d)+(?:,\s?([^\)]+))?\)/i
        //$param_pattern = "/{{$element->key}\((\d)+(?:,\s?([^\)]+))\)}/i";
        $this->CI->load->library('Template/TemplateFunctionFilter');
        $replacement = array();
        $param_pattern = "/{{$element->key}(?:\((\d)+(?:,\s?([^\)]+))?\))?}/i";
        preg_match_all($param_pattern, $file_content, $result_array);
        
        if (count(array_unique($result_array[0])) > 0) {
            for ($i = 0; $i < count($result_array[0]); $i++) {
                $replacement[$i] = $this->CI->templatefunctionfilter->filterFunction($element);
            }
        }
        //does all replacement here
        $file_content = str_replace($result_array[0], $replacement, $file_content);
        return $file_content;
    }

}