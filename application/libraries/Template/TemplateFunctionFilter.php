<?php

class TemplateFunctionFilter {

    public function filterFunction($element) {

        //check each filtered result from above and run the function
        /*
         * Require optimization
         */
        switch ($element->key) {
            case 'test':
                return $this->test();

            case 'adminName':
                return $this->adminName();

            case 'add':
                return $this->add($result_array[1][$i], $result_array[2][$i]);
        
            case 'cwd':
                return $path = getcwd();
            
            case 'baseUrl':
                return $base = base_url();
            case 'validationError':
                return validation_errors();
        }
    }

    private function test() {
        return 'test worked';
    }

    private function adminName() {
        return 'Jason';
    }

    private function add($n, $m) {
        return $n + $m;
    }

}