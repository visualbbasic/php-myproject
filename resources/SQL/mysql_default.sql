

CREATE TABLE IF NOT EXISTS user(
	userID VARCHAR(35) NOT NULL UNIQUE,
	password VARCHAR(40) NOT NULL,
	nickname VARCHAR(30) NOT NULL UNIQUE,
	email VARCHAR(30) NOT NULL UNIQUE,
	facebook VARCHAR(50) DEFAULT "Not specified",
	twitter VARCHAR(50) DEFAULT "Not specified",
	numSiteVisits BIGINT DEFAULT 0,
	iconPath VARCHAR(100) DEFAULT NULL,
	userDescription VARCHAR(450),
	registerDate DATE,
	totalVisits INTEGER DEFAULT 0,
	country VARCHAR(20),
	gradeID INTEGER,
	CONSTRAINT user_PKEY PRIMARY KEY(userID),
	CONSTRAINT user_FKEY1 FOREIGN KEY(gradeID) REFERENCES userGrade(gradeID)
);


CREATE TABLE IF NOT EXISTS userGrade(
	gradeID INTEGER,
	gradeName VARCHAR(50) DEFAULT "New Member",
	writePost BIT NOT NULL DEFAULT 0,
	readPost BIT NOT NULL DEFAULT 0,
	deletePost BIT NOT NULL DEFAULT 0,
	updatePost BIT NOT NULL DEFAULT 0,
	writeComment BIT NOT NULL DEFAULT 0,
	readComment BIT NOT NULL DEFAULT 0,
	deleteComment BIT NOT NULL DEFAULT 0,
	updateComment BIT NOT NULL DEFAULT 0,
	CONSTRAINT userGrade_PKEY PRIMARY KEY (gradeID)
);

CREATE TABLE IF NOT EXISTS page(
	pageID INTEGER NOT NULL UNIQUE,
	numPageVisits BIGINT DEFAULT 0,
	pageName VARCHAR(40) NOT NULL UNIQUE,
	dateCreated DATE,
	category VARCHAR(40) DEFAULT NULL,
	CONSTRAINT page_PKEY PRIMARY KEY (pageID) 
);


CREATE TABLE IF NOT EXISTS post(
	postID INTEGER NOT NULL UNIQUE,
	postTitle VARCHAR(100) NOT NULL,
	postType VARCHAR(20) NOT NULL DEFAULT "Normal",
	postPassword VARCHAR(10) NOT NULL,
	views BIGINT DEFAULT 0,
	thumbUp INTEGER DEFAULT 0,
	thumbDown INTEGER DEFAULT 0,
	postDate DATE NOT NULL,
	postBody VARCHAR(20000) CHARACTER SET UTF8,
	userID VARCHAR(30) DEFAULT "Guest",
	userIPAddress VARCHAR(30) NOT NULL,
	pageID INTEGER NOT NULL,
	CONSTRAINT post_PKEY PRIMARY KEY (postID),
	CONSTRAINT post_FKEY1 FOREIGN KEY(userID) REFERENCES user(userID),
	CONSTRAINT post_FKEY2 FOREIGN KEY(pageID) REFERENCES page(pageID) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS comment(
	commentID INTEGER NOT NULL UNIQUE,
	commentTitle VARCHAR(100) NOT NULL,
	commentBody VARCHAR(500) NOT NULL,
	postDate DATE NOT NULL,
	userIPAddress VARCHAR(30) NOT NULL,
	userID VARCHAR(35) DEFAULT "Guest",
	postID INTEGER NOT NULL,
	CONSTRAINT comment_PKEY PRIMARY KEY(commentID),
	CONSTRAINT comment_FKEY1 FOREIGN KEY(userID) REFERENCES user(userID),
	CONSTRAINT comment_FKEY2 FOREIGN KEY(postID) REFERENCES post(postID) ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS privateMessage(
	messageID INTEGER NOT NULL UNIQUE,
	messageTo VARCHAR(35) NOT NULL,
	messageFrom VARCHAR(35) NOT NULL,
	messageTitle VARCHAR(100) NOT NULL,
	messageBody VARCHAR(800) NOT NULL,
	sentDate DATE NOT NULL,
	CONSTRAINT privateMessage_PKEY PRIMARY KEY(messageID),
	CONSTRAINT privateMessage_FKEY1 FOREIGN KEY(messageTo) REFERENCES user(userID),
	CONSTRAINT privateMessage_FKEY2 FOREIGN KEY(messageFrom) REFERENCES user(userID)
);



CREATE TABLE IF NOT EXISTS email(
	emailID INTEGER NOT NULL UNIQUE,
	emailTo VARCHAR(100) DEFAULT "Unknown",
	emailFrom VARCHAR(100) DEFAULT "UnKnown",
	emailDate DATE NOT NULL,
	title VARCHAR(100) DEFAULT "Un-specified",
	emailBody VARCHAR(20000),
	CONSTRAINT email_PKEY PRIMARY KEY(emailID)
);